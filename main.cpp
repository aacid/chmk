/*
 *  Copyright (C) 2020 Albert Astals Cid <aacid@kde.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chmk.h"

#include "chmk_version.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QDir>
#include <QPointer>
#include <QUrl>
#include <QWebEngineUrlScheme>

#include <KAboutData>
#include <KCrash>
#include <KLocalizedString>

int main(int argc, char **argv)
{
    QWebEngineUrlScheme scheme("ms-its");
    scheme.setSyntax(QWebEngineUrlScheme::Syntax::Host);
    scheme.setFlags(QWebEngineUrlScheme::LocalAccessAllowed	);
    QWebEngineUrlScheme::registerScheme(scheme);

    QApplication a(argc, argv);

    KLocalizedString::setApplicationDomain("chmk");

    KAboutData aboutData( QStringLiteral("chmk"), i18n("ChmK"), CHMK_VERSION_STRING, i18n("CHM viewer"), KAboutLicense::GPL, i18n("(c) 2020 The chmk Developers"));
    aboutData.addAuthor(i18n("Albert Astals Cid"), i18n("Maintainer"), QStringLiteral("aacid@kde.org"));
    QCommandLineParser parser;
    KAboutData::setApplicationData(aboutData);
    KCrash::initialize();
    parser.addPositionalArgument(QStringLiteral("chm-file"), i18n("File to open."));

    aboutData.setupCommandLine(&parser);
    parser.process(a);
    aboutData.processCommandLine(&parser);

    QPointer<Chmk> v = new Chmk();
    v->show();

    if (!parser.positionalArguments().isEmpty())
          v->openFile(QUrl::fromUserInput(parser.positionalArguments().at(0), QDir::currentPath()).toLocalFile());

    const int result = a.exec();
    delete v;
    return result;
}
