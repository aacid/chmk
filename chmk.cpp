/*
 *  Copyright (C) 2020 Albert Astals Cid <aacid@kde.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chmk.h"

#include "lib/ebook_chm.h"
#include "lib/chmtocimage.h"

#include <KActionCollection>
#include <KLocalizedString>
#include <KMessageBox>
#include <KStandardAction>

#include <QApplication>
#include <QBuffer>
#include <QDomElement>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QTreeWidget>
#include <QShortcut>
#include <QSplitter>
#include <QWebEngineView>
#include <QWebEngineProfile>
#include <QWebEngineUrlSchemeHandler>
#include <QWebEngineUrlRequestJob>

#include <memory>

class MSITSSchemeHandler : public QWebEngineUrlSchemeHandler
{
public:
    void requestStarted(QWebEngineUrlRequestJob *request) override
    {
        QBuffer *device = new QBuffer();
        if (!m_file->getFileContentAsBinary(device->buffer(), request->requestUrl()))
        {
            qDebug() << "getBinaryContent failed" << request->requestUrl();
            request->fail(QWebEngineUrlRequestJob::UrlNotFound);
            return;
        }

        request->reply("text/html", device);
        connect(request, &QObject::destroyed, device, &QObject::deleteLater);
    }

    void setFile(EBook_CHM *file)
    {
        m_file.reset(file);
    }

private:
    std::unique_ptr<EBook_CHM> m_file;
};

Chmk::Chmk()
{
    m_topicsView = new QTreeWidget(this);
    m_topicsView->setHeaderLabel(i18n("Contents"));

    m_indexView = new QTreeWidget(this);
    m_indexView->setHeaderLabel(i18n("Index"));

    QWidget *rightWidget = new QWidget(this);
    m_view = new QWebEngineView(this);
    m_findEdit = new QLineEdit(rightWidget);
    m_findEdit->setPlaceholderText(i18n("Find..."));
    m_findEdit->hide();

    QVBoxLayout *rightLayout = new QVBoxLayout(rightWidget);
    rightLayout->addWidget(m_view);
    rightLayout->addWidget(m_findEdit);
    rightLayout->setContentsMargins(0, 0, 0, 0);

    QSplitter *w = new QSplitter();
    w->addWidget(m_topicsView);
    w->addWidget(m_indexView);
    w->addWidget(rightWidget);

    setCentralWidget(w);

    // TODO save/restore the sizes from config
    const QList<int> splitterSizes = { 50, 50, 500 };
    w->setSizes(splitterSizes);

    KStandardAction::open(this, &Chmk::showOpenFileDialog, actionCollection());
    KStandardAction::quit(qApp, &QApplication::quit, actionCollection());
    m_backAction = KStandardAction::back(this, &Chmk::goBack, actionCollection());
    m_forwardAction = KStandardAction::forward(this, &Chmk::goForward, actionCollection());
    m_findAction = KStandardAction::find(this, &Chmk::showFindBar, actionCollection());

    m_findAction->setEnabled(false);

    QAction *viewBackAction = m_view->pageAction(QWebEnginePage::Back);
    auto syncBackActionEnabled = [this, viewBackAction] {
        m_backAction->setEnabled(viewBackAction->isEnabled());
    };
    syncBackActionEnabled();
    connect(viewBackAction, &QAction::changed, this, syncBackActionEnabled);

    QAction *viewForwardAction = m_view->pageAction(QWebEnginePage::Forward);
    auto syncForwardActionEnabled = [this, viewForwardAction] {
        m_forwardAction->setEnabled(viewForwardAction->isEnabled());
    };
    syncForwardActionEnabled();
    connect(viewForwardAction, &QAction::changed, this, syncForwardActionEnabled);

    setupGUI(ToolBar | Keys | Save | Create);

    m_handler = new MSITSSchemeHandler();
    QWebEngineProfile::defaultProfile()->installUrlSchemeHandler("ms-its", m_handler);

    connect(m_topicsView, &QTreeWidget::itemActivated, this, &Chmk::topicItemActivated);

    connect(m_indexView, &QTreeWidget::itemActivated, this, &Chmk::indexItemActivated);

    connect(m_findEdit, &QLineEdit::returnPressed, this, &Chmk::doSearch);
    QShortcut *escPressed = new QShortcut(Qt::Key_Escape, m_findEdit);
    connect(escPressed, &QShortcut::activated, this, &Chmk::closeSearch);

    m_view->setFocus();
}

void Chmk::showOpenFileDialog()
{
    const QStringList mimeTypeFilters({"application/x-chm",
                                       "application/octet-stream" // All files
                                      });

    QFileDialog dialog(this);
    dialog.setMimeTypeFilters(mimeTypeFilters);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    if (dialog.exec()) {
        const QStringList files = dialog.selectedFiles();
        if (files.isEmpty()) {
            return;
        }

        openFile(files[0]);
    }
}

void setBold(QTreeWidgetItem *twi, bool bold)
{
    QFont f = twi->font(0);
    f.setBold(bold);
    twi->setFont(0, f);
}

void Chmk::openFile(const QString &filePath)
{
    EBook_CHM *file = static_cast<EBook_CHM *>(EBook::loadFile( filePath ));
    m_handler->setFile(file);

    m_topicsView->clear();
    m_indexView->clear();

    if (!file) {
        KMessageBox::error(widget(), i18n("Could not open %1", filePath));
        return;
    }

    m_findAction->setEnabled(true);

    QList< EBookTocEntry > topics;
    file->getTableOfContents(topics);

    QString firstPageUrl;
    QMap<int, QTreeWidgetItem *> lastIndentItem;
    for (const EBookTocEntry &e : qAsConst(topics))
    {
        QTreeWidgetItem *twi;
        if (e.indent == 0) {
            twi = new QTreeWidgetItem(m_topicsView);
        } else {
            twi = new QTreeWidgetItem(lastIndentItem[e.indent -1]);
        }
        twi->setText(0, e.name);
        if (!e.url.isEmpty())
        {
            const QString url = e.url.toString();
            twi->setData(0, Qt::UserRole, url);
            if (firstPageUrl.isEmpty())
            {
                firstPageUrl = url;
                setBold(twi, true);
                m_currentViewItem = twi;
            }
        }
        twi->setData(0, Qt::DecorationRole, LCHMTocImage::getImage(e.iconid));
        lastIndentItem[e.indent] = twi;
    }

    QList<EBookIndexEntry> index;
    file->getIndex(index);
    for (const EBookIndexEntry &e : qAsConst(index))
    {
        QTreeWidgetItem *twi;
        if (e.indent == 0) {
            twi = new QTreeWidgetItem(m_indexView);
        } else {
            twi = new QTreeWidgetItem(lastIndentItem[e.indent -1]);
        }
        twi->setText(0, e.name);
        twi->setData(0, Qt::UserRole, QVariant::fromValue<QList<QUrl>>(e.urls));
        // TODO find a file with e.seealso and decide what to do with it
        lastIndentItem[e.indent] = twi;
    }


    if (firstPageUrl.isEmpty()) {
        KMessageBox::error(this, i18n("Could not find the first page of the document. Please open a bug in %1", QStringLiteral("<a href=\"https://bugs.kde.org\">bugs.kde.org</a>")), {}, KMessageBox::Notify | KMessageBox::AllowLink);
    }

    m_view->setUrl(firstPageUrl);
}

void Chmk::goBack()
{
    m_view->back();
}

void Chmk::goForward()
{
    m_view->forward();
}

void Chmk::topicItemActivated(QTreeWidgetItem *twi)
{
    const QString url = twi->data(0, Qt::UserRole).toString();
    if (url.isEmpty()) {
        if (twi->childCount() > 0) {
            twi->setExpanded(true);
            topicItemActivated(twi->child(0));
        }
        return;
    }

    m_view->setUrl(url);
    setBold(m_currentViewItem, false);
    setBold(twi, true);
    m_currentViewItem = twi;
}

void Chmk::indexItemActivated(QTreeWidgetItem *twi)
{
    const QList<QUrl> urls = twi->data(0, Qt::UserRole).value<QList<QUrl>>();

    if (urls.isEmpty())
        return;

    // TODO What to do when there's more than 1 url?
    m_view->setUrl(urls[0]);
}

void Chmk::showFindBar()
{
    m_findEdit->show();
    m_findEdit->setFocus();
}

void Chmk::doSearch()
{
    m_view->page()->findText(m_findEdit->text());
}

void Chmk::closeSearch()
{
    m_view->findText(QString());
    m_view->pageAction(QWebEnginePage::Unselect)->trigger();
    m_findEdit->hide();
    m_findEdit->clear();

}
