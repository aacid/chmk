/*
 *  Copyright (C) 2020 Albert Astals Cid <aacid@kde.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <KXmlGuiWindow>

class MSITSSchemeHandler;

class QAction;
class QLineEdit;
class QTreeWidget;
class QTreeWidgetItem;
class QWebEngineView;

class Chmk : public KXmlGuiWindow
{
public:
    Chmk();

    void openFile(const QString &filePath);

    void showOpenFileDialog();

    void goBack();
    void goForward();

    void topicItemActivated(QTreeWidgetItem *twi);
    void indexItemActivated(QTreeWidgetItem *twi);

    void showFindBar();
    void doSearch();
    void closeSearch();

private:
    QTreeWidget *m_topicsView;
    QTreeWidget* m_indexView;
    QTreeWidgetItem *m_currentViewItem;
    QWebEngineView *m_view;
    MSITSSchemeHandler *m_handler;

    QAction *m_backAction;
    QAction *m_forwardAction;

    QLineEdit *m_findEdit;
    QAction *m_findAction;
};
